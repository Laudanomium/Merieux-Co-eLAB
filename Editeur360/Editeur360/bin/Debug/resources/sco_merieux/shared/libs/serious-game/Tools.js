/**
 * @class
 * @listens PhotoSphereViewer#click
 * @listens PhotoSphereViewer#select-maker
 * @listens PhotoSphereViewer#unselect-maker
 * @listens scene-change
 * @listens document#keypress
 */
function Tool(serious_game) {
    /**
     * @type {SeriousGame}
     * @readonly
     */
	this.serious_game = serious_game;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onClick = null;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onSelectMarker = null;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onUnselectMarker = null;

    /**
     * @callback
     * @this null
     */
	this.onKeyPress = null;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onSelect = null;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onDeselect = null;

    /**
     * @callback
     * @this seriousgame.PSV
     */
	this.onSceneChange = null;

    /**
     * @type {string}
     * @readonly
     * @constant
     */
	this.description = "Tools";

    /**
     * @type {string}
     * @readonly
     * @constant
     */
	this.icon = null;
}

/**
 * @function
 */
Tool.prototype.select = function() {
	if (this.onClick != null) {
		this.serious_game.PSV.on('click', this.onClick);
	}
	
	if (this.onSelectMarker != null) {
		this.serious_game.PSV.on('select-marker', this.onSelectMarker);
	}

	if (this.onUnselectMarker != null) {
		this.serious_game.PSV.on('unselect-marker', this.onUnselectMarker);
	}

	if (this.onKeyPress != null) {
		document.addEventListener("keypress", this.onKeyPress);
	}

	if (this.onSceneChange != null) {
		this.serious_game.PSV.on("scene-change", this.onSceneChange);
	}

	if (this.onSelect != null) {
		this.onSelect();
	}
}

/**
 * @function
 */
Tool.prototype.deselect = function() {
	if (this.onClick != null) {
		this.serious_game.PSV.off('click', this.onClick);
	}
	
	if (this.onSelectMarker != null) {
		this.serious_game.PSV.off('select-marker', this.onSelectMarker);
	}

	if (this.onUnselectMarker != null) {
		this.serious_game.PSV.off('unselect-marker', this.onUnselectMarker);
	}

	if (this.onKeyPress != null) {
		document.removeEventListener("keypress", this.onKeyPress);
	}

	if (this.onSceneChange != null) {
		this.serious_game.PSV.off("scene-change", this.onSceneChange);
	}

	if (this.onDeselect != null) {
		this.onDeselect();
	}
}


/**
 * @class
 * @implements Tool
 */
function MarkerTool(serious_game) {
	Tool.call(this, serious_game);

	this.description = "Dessiner une zone";
	this.icon = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';

	var self = this;

    /**
     * @override
     */
	this.onKeyPress = function(e) {
		if (serious_game.is_key("enter", e.which)) {
			serious_game.validate_current_polygon();
		}
	}

    /**
     * @override
     */
	this.onClick = function(e) {
		if (e.marker && !e.marker.isPolygon()) {
			return;
		}

		serious_game.add_polygon_vertex({
			longitude: e.longitude,
			latitude: e.latitude,
			x: e.texture_x,
			y: e.texture_y
		});
	}
}

MarkerTool.prototype = Object.create(Tool.prototype);
MarkerTool.constructor = MarkerTool;

/**
 * @class
 * @implements Tool
 */
function RemoveMarkerTool(serious_game) {
	Tool.call(this, serious_game);

	this.description = "Retirer une zone ou un sommet";
	this.icon = '<i class="fa fa-eraser" aria-hidden="true"></i>';

	var self = this;

    /**
     * @override
     */
	this.onSelectMarker = function(marker) {
		if (!marker.data.deletable) {
			return;
		}
		
		if (marker.isPolygon()) {
			// Open panel
		}
		else {
			if (marker.id.startsWith("#info_pin_id:"))
			{
				// Open panel
			}
			else {
				serious_game.remove_vertex(marker);
			}
		}
	}
}

RemoveMarkerTool.prototype = Object.create(Tool.prototype);
RemoveMarkerTool.constructor = RemoveMarkerTool;

/**
 * @class
 * @implements Tool
 */
function Tracker(serious_game) {
	Tool.call(this, serious_game);

    /**
     * @type {Object[]}
     * @readonly
     */
	this.history = [];
	this.description = "Tracking";

	var self = this;

    /**
     * @override
     */
	this.onSelectMarker = function(e) {
		var name = null;

		if (e.isPolygon()) {
			name = e._name;
		}
		else {
			var polygon_id = e.id.replace("#info_pin_id: ", "");
			var polygon = self.serious_game.PSV.getMarker("#polygon_id: " + polygon_id);

			name = polygon._name;
		}
		
		self.history[self.history.length - 1].polygons.push(name);
	}

    /**
     * @override
     */
	this.onSceneChange = function(old_id, new_id) {
		if (self.history.length) {
			var last_record = self.history[self.history.length - 1];

			if (last_record.scene == new_id) {
				return;
			}
		}
		
		self.history.push({
			scene: new_id,
			polygons: []
		});
	}

    /**
     * @returns {Object[]}
     */
	this.getVisitedScenes = function() {
		var visited_scenes = [];
		
		for (var i in this.history) {
			var scene = this.history[i].scene;
			
			if (visited_scenes.indexOf(scene) == -1) {
				visited_scenes.push(scene);
			}
		}

		return visited_scenes;
	}
}

Tracker.prototype = Object.create(Tool.prototype);
Tracker.constructor = Tracker;
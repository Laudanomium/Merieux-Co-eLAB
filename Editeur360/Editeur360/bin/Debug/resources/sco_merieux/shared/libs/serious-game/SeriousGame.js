/*
  This is a full application (serious game) to edit and play levels
  stored locally (localStorage/json file) or through a SCORM package.
*/
var DEBUG_MODE = 0;				// Set to true, display polygons even in user mode.

// http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
var get_property = function(o, s) {
	s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
	s = s.replace(/^\./, '');           // strip a leading dot
	var a = s.split('.');
	for (var i = 0, n = a.length; i < n; ++i) {
		var k = a[i];
		if (k in o) {
			o = o[k];
		} else {
			return undefined;
		}
	}
	return o;
}

var set_property = function(o, s, v) {
	s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
	s = s.replace(/^\./, '');           // strip a leading dot
	var a = s.split('.');
	for (var i = 0, n = a.length; i < n - 1; ++i) {
		var k = a[i];
		if (k in o) {
			o = o[k];
		} else {
			o[k] = {};
			o = o[k];
		}
	}

	o[a[n - 1]] = v;
}

// Types

/**
 * @typedef {Object} vertex - {@link http://photo-sphere-viewer.js.org/api/PSVMarker.html}
 */
/**
 * @typedef {Object} polygon - {@link http://photo-sphere-viewer.js.org/api/PSVMarker.html}
 */


// Events
/**
 * Answer quiz event.
 * 
 * @event answer-quizz
 * @property {string} polygon_id - short polygon id
 */
/**
 * Scene change event.
 * 
 * @event scene-change
 * @property {string} old_scene_id
 * @property {string} new_scene_id
 */
/**
 * Level complete event.
 * 
 * @event level-complete
 */
/**
 * Partial level complete event.
 * 
 * @event partial-level-complete
 */

/**
 * Main namespace for the serious game. Everything related to the
 * edition and the interactions with the level is done here.
 * @namespace
 */
var SeriousGame = {
	/**
     * Namespace for constants.
     * @namespace
	 */
    Constants: {
		/**
		 * Default properties of the markers (polygons vertices).
		 * @constant
		 */
        MarkerDefaultProperties: {
            width: 18,
            height: 18
        },

		/**
		 * Key name to key code map.
		 * @ignore
		 */
        KeycodeMap: {
            enter: 13
        },

		/**
		 * Directories for each type of element which can be accessed
		 * locally.
		 * @readonly
		 * @enum {string}
		 * @see SeriousGame.get_file
		 */
		Directories: {
			/** Icons */
			InfoType: "./assets/icons",
			/** General assets (PNG, JPG, ...) */
			Asset:    "./assets",
			/** Panoramas */
			Panorama: "./assets",
			/** Others (HTML, ...) */
			Other:    "./assets",
			/** Json formatted levels */
			Level:    "."
		},

		/**
		 * Dictionnary containing icons that can be used for the
		 * polygon (keys are the names displayed in the selection).
		 * IMPORTANT: filenames must no contain spaces.
		 * @constant
		 */
		InfoTypes: {
			// inserer les images ici #insertion_icones
			"Attention": "Attention.png",
			"Bloc note": "Bloc_note.png",
			"Bombe": "bombe.png",
			"Oeil": "oeil.png",
			"Punaise bleue": "punaise_bleue.png",
			"Punaise noire": "punaise_noire.png",
			"Punaise rouge": "punaise_rouge.png"
		},

		
		/**
		 * Every base model which are modified according to the
		 * currently selected polygon/scene.
		 * @constant
		 */
		Templates: {
			// Main model used both in user (when polygon has not been
			// completed) and admin mode (the name is the same, models
			// are different).
			polygon_template: document.getElementById("polygon_template_#id"),

			// Model used in admin mode which corresponds to the
			// 'content' definition (associated images, quiz, pdf,
			// ...)
			polygon_personalisation_template: document.getElementById("polygon_personalisation_template_#id_#count"),
			
			// Model used in user mode when a polygon has been
			// completed.
			polygon_completed_template: document.getElementById("polygon_completed_template_#id"),

			// Model used in admin mode when displaying the 'Goto
			// scene' menu. It's the miniature representation of the
			// scene.
			scene_miniature_template: document.getElementById("scene_miniature_template_#id")
		},

		// Every id inside polygon_template starting with a '_'.
		PolygonEditableFields: null
    },

	/**
     * Initialize SeriousGame. Custom properties can later be
     * accessed in SeriousGame.config.
	 * @param {Object} properties
	 * @see http://photo-sphere-viewer.js.org/#options
     * @listens PhotoSphereViewer#ready
     * @listens PhotoSphereViewer#close-panel
     * @listens PhotoSphereViewer#open-panel
     * @listens PhotoSphereViewer#select-marker
     * @listens PhotoSphereViewer#render-markers-list
	 */
    init: function (properties) {
		this.level_id = properties.level_id;

		var self = this;
		var get_polygon_editable_fields = function() {
			var all_references = self.Constants.Templates.polygon_template.innerHTML.match(/(["']polygon_)(?!content)([a-z]+)/g)

			var toto = [];

			for (var i in all_references) {
				toto.push(all_references[i].substring(9));
			}

			return toto;
		};

		this.Constants.PolygonEditableFields = get_polygon_editable_fields();

		var navbar = null;
		
        if (properties.edition) {
            navbar = this._init_edition_mode(properties);
        }
        else {
            navbar = this._init_play_mode(properties);
        }

        this.config = properties;		

		this._load_level_data(properties);

		// TODO: Move some of this config to _load_scene.
		var psv_config =  {
			container: 'photosphere',
			loading_img: this.get_file("Asset", 'merieux.png'),
			latitude_range: [-3*Math.PI/4, 3*Math.PI/4],
			anim_speed: '-2rpm',
			default_fov: 50,
			fisheye: false,
			move_speed: 1.1,
			time_anim: false,
			gyroscope: true,
			webgl: true,
			transition: {
				duration: 1000,
				loader: true,
				blur: false
			},
			navbar: navbar
		};
		
		var scene_config = null;
		var default_scene = this._get_level_default_scene();

		if (default_scene == null) {
			scene_config = {panorama: this.get_file("Asset", 'no_scene_background.png')};
		}
		else {
			if (properties.from_json) {                
				scene_config = JSON.parse(JSON.stringify(this._all_scenes[this._all_scene_ids.indexOf(default_scene)]));            
			}            
			else {                
				scene_config = JSON.parse(localStorage.getItem(this.level_id + "_" + default_scene));
			}            
			scene_config.panorama = this.get_file("Panorama", scene_config.panorama);
		}

		PhotoSphereViewer.Utils.deepmerge(psv_config, scene_config);

        this.PSV = new PhotoSphereViewer(psv_config);

		this.PSV.on("ready", function() {
			if (!this.config.edition) {
				this._tracker.select();
			}
			
			this._start_level();
		}.bind(this));
 
		if (this.config.edition) {
			this.PSV.on("close-panel", function() {
				if (this._currently_edited_polygon) {
					try  {
						this.PSV.getMarker(this._currently_edited_polygon.ref.id);
						
						var polygon = this._currently_edited_polygon.ref;

						polygon._content = this._currently_edited_polygon._content;
						polygon.content = this._generate_polygon_content(polygon);
						
						this._currently_edited_polygon = null;
						
						this.PSV.updateMarker(polygon, true);
					}
					catch(e) {
						// Polygon was deleted during edition.
					}
				}

				this._currently_edited_polygon = null;
			}.bind(this));

			this.PSV.on('select-marker', function(polygon) {
				if (polygon.isPolygon()) {
					this._currently_edited_polygon = {
						ref: polygon,
						_content: JSON.parse(JSON.stringify(polygon._content))
					}
				}
			}.bind(this));
		}
		else {
			this.PSV.on("open-panel", function() {
				var marker = this.PSV.getCurrentMarker();

				if (marker == null) {
					return;
				}

				var polygon_id = null;
				var polygon = null;

				if (marker.isPolygon()) {
					polygon_id = marker.id.replace("#polygon_id: ", "");
					polygon = marker;
				}
				else {
					polygon_id = marker.id.replace("#info_pin_id: ", "");
					polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);
				}

				var all_references = this.Constants.Templates.polygon_template.innerHTML.match(/\$.[^_" <\/]+/g);

				for (var i in all_references) {
					try {
						var id = all_references[i] + "_" + polygon_id;
						
						var field = document.getElementById(id);
						var property = "_" + all_references[i].substring(1);
						var value = get_property(polygon, property);
						
						if (!value ||
							((property == "_destination") &&
							 (value == "aucune"))) {
							field.style.display = 'none';
						}
					}
					catch(e) {
						console.log(e);
					}
				}

				if ((polygon._destination != "aucune") ||
					get_property(polygon, "_content.quizz.question")) {
					var acknowledge_button = document.getElementById("acknowledge_" + polygon_id);

					if (acknowledge_button) {
						acknowledge_button.style.display = 'none';
					}
				}
			}.bind(this));
		}

		this.PSV.on('render-markers-list', function(markers) {
			var polygons = [];

			for (var i in markers) {
				if (markers[i].isPolygon()) {
					polygons.push(markers[i]);
				}
			}

			return polygons;
		});

		try {
			this.PSV.render();
		}
		catch(e) {
			console.log(e);
		}
    },

	/**
	 * Return true if the key represented by name has the same code as
	 * keycode.
	 * @param {string} name - name
	 * @param {number} keycode - keycode
	 * @returns {boolean}
	 */
	is_key: function (name, keycode) {
        return this.Constants.KeycodeMap[name] == keycode;
    },

	/**
	 * Return the full relative path of a file based on it's type.
	 * @example
	 * get_file("Panorama", "my_panorama.png")
	 * @param {string} type - key in {@link SeriousGame.Constants.Directories}
	 * @param {string} filename - filename
	 * @returns {string}
     * @see SeriousGame.Constants.Directories
	 */
	get_file: function(type, filename) {
		return this.Constants.Directories[type] + '/' + filename;  
	},

	/**
	 * Add a new polygon vertex.
	 * @param {Object} properties - properties of the vertex
	 * @returns {vertex} 
	 */
    add_polygon_vertex: function (properties) {
        var vertex_properties = {
            id: "#polygon_id: " + this._polygon_id + " #vertex_id: " + this._vertex_id,
            image: this.get_file('Asset', 'Red_pog.svg'),
            width: this.Constants.MarkerDefaultProperties.width,
            height: this.Constants.MarkerDefaultProperties.height,
            anchor: 'center center',
            data: {
                deletable: true,
                selectable: true
            }
        };

        PhotoSphereViewer.Utils.deepmerge(vertex_properties, properties);

        var new_vertex = this.PSV.addMarker(vertex_properties);

        this._polygon_vertices.push(new_vertex);

        this._vertex_id++;

		if (this._polygon_vertices.length > 2) {
			this.PSV.getNavbarButton("button_validate_polygon").enable();
		}

        return new_vertex;
    },

	/**
	 * Combine the given vertices to create a new polygon and remove
	 * them.
	 * @param {vertex[]} vertices
	 * @param {Object} properties - properties of the polygon
	 * @returns {polygon} 
	 */
	add_polygon: function (vertices, properties) {
        var polygon_points = [];

        for (var i = 0; i < vertices.length; ++i) {
            var vertex = vertices[i];

            this.PSV.removeMarker(vertex, false);

            polygon_points.push([vertex.x, vertex.y]);
        }

        var polygon_properties = {
            id: "#polygon_id: " + this._polygon_id,
			_scene_id: this._scene_id,

			className: "polygon",

            svgStyle: {
                fill: 'url(#points)',
                stroke: 'rgba(255, 0, 50, 0.8)',
                'stroke-width': '2px'
            },

            tooltip: {
                content: 'Indéfini',
                position: 'right center'
            },

            data: {
                deletable: true,
                selectable: true
            },

			_content: [],

            // polygon_vertices: this._polygon_vertices.slice(0),
            polygon_px: polygon_points.slice(0)
        };

		for (i in this.Constants.PolygonEditableFields) {
			polygon_properties["_" + this.Constants.PolygonEditableFields[i]] = "";
		}

        PhotoSphereViewer.Utils.deepmerge(polygon_properties, properties);

		// Compute bounding box
		var min_x = Number.MAX_VALUE;
		var max_x = Number.MIN_VALUE;
		var min_y = Number.MAX_VALUE;
		var max_y = Number.MIN_VALUE;
		
		for (i in polygon_properties.polygon_px) {
			var point = polygon_properties.polygon_px[i];
			
			min_x = Math.min(min_x, point[0]);
			max_x = Math.max(max_x, point[0]);
			
			min_y = Math.min(min_y, point[1]);
			max_y = Math.max(max_y, point[1]);
		}
		
		polygon_properties.bounding_box = [[min_x, min_y], [max_x, max_y]];
		
		var new_polygon = this.PSV.addMarker(polygon_properties);
		new_polygon.content = this._generate_polygon_content(new_polygon)

		this._polygon_id++;

        return new_polygon;
    },

	/**
	 * Combine every remaining vertices into a polygon and remove
	 * them.
	 * @param {Object} properties - properties of the polygon
	 * @returns {polygon} 
	 */
    validate_current_polygon: function (properties) {
        if (!(this._polygon_vertices.length > 2)) {
            return null;
        }

        var new_polygon = this.add_polygon(this._polygon_vertices, properties);

        this._polygon_vertices = [];
        this._vertex_id = 0;

		this.PSV.getNavbarButton("button_validate_polygon").disable();

        return new_polygon;
    },

	/**
	 * Remove the given vertex.
	 * @param {vertex} vertex
	 */
    remove_vertex: function (vertex) {
        var index = this._polygon_vertices.indexOf(vertex);
		
        if (index > -1) {
            this._polygon_vertices.splice(index, 1);
			
			if (this._polygon_vertices.length < 3) {
				this.PSV.getNavbarButton("button_validate_polygon").disable();
			}
        }

        // NOTE: Vertex may have been removed along with
        //       another one already.
        //
        try {
            this.PSV.removeMarker(vertex, false);
        }
        catch (e) {
        }
    },

	/**
	 * Remove the given polygon and it's potential icon (if
	 * confirmed).
	 * @param {polygon} polygon
	 */
	remove_polygon: function(polygon) {
		if (confirm("Êtes-vous de vouloir supprimer cette zone ?")) {
			try {
				var polygon_id = null;
				var id = null;
					
				if (typeof(polygon) == "string") {
					polygon_id = polygon;
					id = "#polygon_id: " + polygon_id;
					
					polygon = this.PSV.getMarker(id);
				}
				else {
					polygon_id = polygon.id.replace("#polygon_id: ", "");
					id = polygon.id;
				}

				if (polygon._infotype &&
					(polygon._infotype != "aucun")) {
					var pin_id = "#info_pin_id: " + polygon_id;

					try {
						this.PSV.removeMarker(pin_id);
					}
					catch(e) {
						console.log(e);
					}
				}

				polygon.content = "";
				
				this.PSV.removeMarker(id);
				this.PSV.hidePanel();
			}
			catch(e) {
				console.log(e);
			}
		}
	},

	/**
     * Set a non-quiz polygon to completed.
     * @example
     * acknowledge_polygon('0')
     * @param {string} polygon_id - short polygon id
     */
	acknowledge_polygon: function(polygon_id) {
		try {
			this._set_polygon_completed(this.PSV.getMarker("#polygon_id: " + polygon_id));
		}
		catch (e) {
			console.log(e);
		}
	},

	/**
     * Set a quiz polygon to completed and compute it's score.
     * @param {string} polygon_id - short polygon id
     * @fires answer-quizz
     */
	submit_quizz_answers: function(polygon_id) {
		var polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);

		if (polygon._completed) {
			return;
		}
		
		var max_answer_count = Object.keys(polygon._content.quizz.answers).length;

		var all_answers = [];
			
		for (var count = 0; count < max_answer_count; ++count) {
			try {
				var answer = document.getElementById("polygon_quizz_answer_" + count + "_" + polygon_id);
				
				all_answers.push({
					valid: answer.checked
				});
			}
			catch(e) {
			}
		}

		var score = this._compute_polygon_score(polygon, all_answers, max_answer_count);
		
		this._set_polygon_completed(polygon, all_answers, score);
		
		++this._total_question_count;
		this._total_score += score * parseInt(polygon._coeff) / this._total_question_coeff;
		
		if (isNaN(this._scene_answer_count[polygon._scene_id])) {
			this._scene_answer_count[polygon._scene_id] = 1;
			this._scene_score[polygon._scene_id] = score * parseInt(polygon._coeff);
		}
		else {
			++this._scene_answer_count[polygon._scene_id];	
			this._scene_score[polygon._scene_id] += score * parseInt(polygon._coeff);
		}

		this.PSV.trigger("answer-quizz", polygon_id);
	},

    /**
     * Suspend the game and save the user's progress.
     * @function
     */
	suspend_game: function() {
		var data = {
			history: this._tracker.history,
			polygons: []
		};

		for (var i in this._all_scenes) {
			var scene = this._all_scenes[i];
			var all_polygons = scene.all_polygons;

			for (var j in all_polygons) {
				var polygon = all_polygons[j];

				if (polygon._content.length == undefined) {
					var polygon_data = {
						scene: polygon._scene_id,
						index: j,
						content_index: polygon._content_index
					}
					
					if (polygon._completed) {
						if (polygon._submittedAnswers) {
							polygon_data.answers =  polygon._submittedAnswers.map(function(x) {return x.valid});
						}
						else {
							if (get_property(polygon, "_content.quizz.question")) {
								polygon_data.answers = [];
							}
						}
					}

					data.polygons.push(polygon_data);
				}
			}
		}

		// TODO: Send to LMS. (And remove localStorage)
		var json = JSON.stringify(data);
		
		localStorage.setItem("suspend_data", json);
	},

    /**
     * Save the currently edited level to localStorage as
     * '<SeriousGame.config.level_id>'.
     * @function
     */
	save_level: function() {
		this._save_current_scene();
		
		var json = JSON.stringify({
			scenes: this._all_scene_ids,
			training: this._training_level
		});

        localStorage.setItem(this.level_id, json);
		this._save_compact_level();
	},

	// Called when the 'Save' button is pressed inside the polygon's
	// panel.
    /**
     * Save the content of the polygon as well the level in
     * localStorage.
     * @param {string} polygon_id - short polygon id
     */
	save_polygon_changes: function(polygon_id) {
		this._update_polygon_content(polygon_id);

		if (this.config.edition) {
			var polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);
			
			this._currently_edited_polygon = {
				ref: polygon,
				_content: JSON.parse(JSON.stringify(polygon._content))
			}
		}

		this.save_level();
	},

    /**
     * Remove the content specified by the given index from the
     * polygon contents. Update the panel.
     * @param {string} polygon_id - short polygon id
     * @param {number} content_index
     */
	remove_polygon_content: function(polygon_id, content_index) {
		try {
			var polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);

			polygon._content.splice(content_index, 1);
			polygon.content = this._generate_polygon_content(polygon);
			this.PSV.panel.content.innerHTML = polygon.content;
			this.PSV.updateMarker(polygon, true);
		}
		catch(e) {
			console.log(e);
		}
	},

    /**
     * Remove the specified element from a polygon content. Update the
     * panel.
     * @example
     * remove_polygon_content_element('0', 'content[1].image'])
     * @param {string} polygon_id - short polygon id
     * @param {string} property - key to access the property 
     */
	remove_polygon_content_element: function(polygon_id, property) {
		try {
			var polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);

			set_property(polygon, "_" + property, "");
			var present_field = document.getElementById("$" + property.replace(".", ".present.") + "_" + polygon_id);
			present_field.style.display = 'none';
		}
		catch(e) {
			console.log(e);
		}
	},
	
	// TODO: Ask to save before leaving (in edition mode).
    /**
     * Change the panorama as well as the displayed polygons. Hide the
     * panel.
     * @param {string} scene_id
     * @listens PhotoSphereViewer#panorama-loaded
     * @fires scene-change
     */
	change_scene_to: function(scene_id) {
		this.PSV.trigger("scene-change", this._scene_id, scene_id);

		this.PSV.panel.content.innerHTML = "";
		this.PSV.hidePanel();

		var data = this._load_scene(scene_id);

		var load_scene_polygons = function() {
			this.PSV.clearMarkers();
			
			for (var i in data.all_polygons) {
				var polygon_data = data.all_polygons[i];
				
				try {
					this._load_polygon(polygon_data, this.config.edition, false);
				}
				catch (e) {
				}
			}

			this.PSV.render();
		}.bind(this);
		
		if (data.panorama != null) {
			this.PSV.setPanorama(this.get_file("Panorama", data.panorama));

			this.PSV.once("panorama-loaded", function() {
				load_scene_polygons();
			}.bind(this));
		}
		else {
			load_scene_polygons();
		}

		this._scene_id = scene_id;

		if (this.config.edition) {
			var set_default_button = this.PSV.getNavbarButton("set_as_default");
			
			if (this._scene_id == this._all_scene_ids[0]) {
				set_default_button.disable();
			}
			else {
				set_default_button.enable();
			}
		}
	},

    /**
     * Show the scene creation panel. On confirm, save the new scene
     * in localStorage as '<SeriousGame.level_id>_<scene_id>'.
     * @function
     */
	create_new_scene: function() {
		var scene_id = document.getElementById("scene_id");
		
		if ((scene_id.value == null) ||
			(scene_id.value == "")) {
			alert("Aucun id donné.");
			return;
		}

		if (this._all_scene_ids.indexOf(scene_id) != -1) {
			alert("L'Id existe déjà.");
			return;
		}
		
		var scene_panorama = document.getElementById("scene_panorama");

		if ((scene_panorama.value == null) ||
			(scene_panorama.value == "")) {
			alert("Aucun panorama donné.");
			return;
		}

		var panorama = scene_panorama.value.substring(scene_panorama.value.lastIndexOf("\\") + 1);

		this._all_scene_ids.push(scene_id.value);

		var data = {
			panorama: panorama,
			all_polygons: []
		};

		var json = JSON.stringify(data);

		localStorage.setItem(this.level_id + "_" + scene_id.value, json);

		this._append_scene_miniature(scene_id.value, data.panorama);
		this.change_scene_to(scene_id.value);
	},

    /**
     * On confirm, remove the given scene from the level data and
     * localStorage. Save the level afterward.
     * @param {string} scene_id
     */
	remove_scene: function(scene_id) {
		if (!confirm("Êtes-vous sûr de vouloir supprimer cette scène ? (Ceci est définitif !)")) {
			return;
		}
		
		var index = this._all_scene_ids.indexOf(scene_id);
		
		this._all_scene_ids.splice(index, 1);

		var remove_scene_from_polygons = function(all_polygons, regenerate_content) {
			for (var p in all_polygons) {
				var polygon = all_polygons[p];

				if (polygon._destination == scene_id) {
					polygon._destination = "";
				}

				if (regenerate_content) {
					polygon.content = this._generate_polygon_content(polygon);
					this.PSV.updateMarker(polygon);
				}
			}
		}.bind(this);

		for (var i in this._all_scene_ids) {
			var scene = this._all_scene_ids[i];

			if (scene == this._scene_id) {
				continue;
			}
				
			var key = this.level_id + "_" + scene;
			
			var json = localStorage.getItem(key);

			if (json == null) continue;

			var data = JSON.parse(json);

			remove_scene_from_polygons(data.all_polygons);

			localStorage.setItem(key, JSON.stringify(data));
		}

		var all_polygons = [];
		for (i = 0; i < this._polygon_id; ++i) {
			try {
				all_polygons.push(this.PSV.getMarker("#polygon_id: " + i));
			}
			catch (e) {
			}
		}
		
		remove_scene_from_polygons(all_polygons, true);

		this._scene_selection_panel = '<h1>Changer de scène</h1>';

		for (i in this._all_scene_ids) {
			this._append_scene_miniature(this._all_scene_ids[i]);
		}
		
		this._show_scene_selection_panel();

		this.save_level();
	},

	/**
     * Unhide the corresponding polygon quiz.
     * @param {string} polygon_id - short polygon id
     */
	show_quizz: function(polygon_id) {
		var all_fields = document.getElementsByName("$content.quizz.question_" + polygon_id);
		
		for (var i in all_fields) {
			var field = all_fields[i];
			
			if (typeof(field) != "object") {
				continue;
			}
			
			if (field.style.display == "none") {
				field.style.display = "";
			}
			else {
				field.style.display = "none";
			}
		}
	},

	

	/* SeriousGame internal variables. */

    /**
     * Contains the successive vertices of a polygon before it is
	 * validated. It's emptied when the current polygon is validated.
     * @type {vertex[]}
     * @private
     */
    _polygon_vertices: [],

     /**
      * Index of the vertex being currently added (set to 0 once the
	  * polygon is validated).
      * @type {number}
      * @private
      */
    _vertex_id: 0,

    /**
     * Index of the polygon being curently added (always increased).
     * @type {number}
     * @private
     */
    _polygon_id: 0,

    /**
     * Currently selected tool.
     * @type {Tool}
     * @private
     */
    _current_tool: null,

    /**
     * Array of the ids of the scene composing the level.
     * @type {string[]}
     * @private
     */
	_all_scene_ids: [],

    /**
     * Array of minimal scene data only used when in user mode
     * IMPORTANT:
     * This should always be in sync with _all_scene_ids (same index
     * in both arrays for the same scene).
     * @type {Object[]}
     * @private
     */
	_all_scenes: null,

    /**
     * HTML representation of the 'Goto scene menu'. (Sorry)
     * Updated when every time a scene is added/removed.
     * @type {string}
     * @private
     */
	_scene_selection_panel: '<h1>Changer de scène</h1>',

    /**
     * User's tracker (Tracker) (only used in user mode).
     * @type {Tracker}
     * @private
     */
	_tracker: null,

    /**
     * Weither the level is in training or not.
     * @type {boolean}
     * @private
     */
	_training_level: false,
	

	/* Variables related to user progression (user mode only) */

    /**
     * Total number of questions (whole-level).
     * @type {number}
     * @private
     */
	_total_question_count: 0,

    /**
     * Sum of the coeff of each quiz-related polygons (whole-level).
     * @type {number}
     * @private
     */
	_total_question_coeff: 0,

    /**
     * @type {number[]}
     * @private
     */
	_scene_question_count: [],

    /**
     * @type {number[]}
     * @private
     */
	_scene_question_coeff: [],

    /**
     * @type {number[]}
     * @private
     */
	_scene_answer_count: [],

    /**
     * @type {number[]}
     * @private
     */
	_scene_answer_coeff: [],

    /**
     * @type {number}
     * @private
     */
	_total_score: 0,

     /**
      * @type {number[]}
      * @private
      */
	_scene_score: [],


    /**
     * @type {string}
     * @private
     */
	level_id: null,

    /**
     * @param {polygon} polygon
     * @param {namespace} utils
     * @returns {string}
     * @private
     */
	_generate_polygon_edition_content: function(polygon, utils) {
		var polygon_id = polygon.id.replace("#polygon_id: ", "");

		// Update polygon_template
		var content = utils.generate_polygon_html(this.Constants.Templates.polygon_template);
		var template_references = utils.get_polygon_references(content);
		
		for (var i in template_references) {
			content = utils.fill_reference(polygon, content, template_references[i]);
		}

		// Update #polygon_infotype_options
		var selected = utils.get_select_status(polygon._infotype, "aucun")
		var polygon_infotype_options = '<option value="aucun"' + selected + '>aucun</option>\n';
		
		for(i in this.Constants.InfoTypes) {
			selected = utils.get_select_status(polygon._infotype, i);
			polygon_infotype_options += '<option value="' + i + '"' + selected + '>' + i + '</option>\n'
		}

		content = content.replace(/_polygon_infotype_options/g, polygon_infotype_options);

		// Update #polygon_coeff_options
		var polygon_coeff_options = '';
		var all_coeffs = [{
			value: "1",
			name: "Normale"
		}, {
			value: "2",
			name: "Haute"
		}, {
			value: "3",
			name: "K.O."
		}];
		
		for(i in all_coeffs) {
			var coeff = all_coeffs[i];
			selected = utils.get_select_status(polygon._coeff, coeff.value);
			polygon_coeff_options += '<option value="' + coeff.value + '"' + selected + '>' + coeff.name + '</option>\n'
		}

		content = content.replace(/_polygon_coeff_options/g, polygon_coeff_options);

		// Update #scene_id_options
		selected = utils.get_select_status(polygon._destination, "aucune");
		var scene_id_options = '<option value="aucune"' + selected + '>aucune</option>\n';
		
		for(i in this._all_scene_ids) {
			var id = this._all_scene_ids[i];

			if (id != polygon._scene_id) {
				selected = utils.get_select_status(polygon._destination, id);
				scene_id_options += '<option value="' + id + '"' + selected + '>' + id + '</option>\n'
			}
		}

		content = content.replace(/_scene_id_options/g, scene_id_options);

		// Update #polygon_personalisation
		var polygon_personalisation_content = utils.generate_polygon_html(this.Constants.Templates.polygon_personalisation_template);
		var all_references = utils.get_polygon_references(polygon_personalisation_content);

		var polygon_personalisation = '';
		for (var count = 0; count <= polygon._content.length; ++count) {
			var new_set = polygon_personalisation_content
				.replace(/#count/g, count)
				.replace(/#ncount/g, count + 1);

			for (i in all_references) {
				var reference = all_references[i].replace(/#count/g, count);
				
				new_set = utils.fill_reference(polygon, new_set, reference);

				var property = reference.substring(1);
				var value = get_property(polygon, "_" + property);

				var present_reference = property.replace(".", ".present.");

				var re = new RegExp("_" + utils.escaped_string(present_reference), 'g');
				new_set = new_set.replace(re, value ? 'visible' : 'none');
			}

			polygon_personalisation += new_set;
		}

		content = content.replace("_polygon_personalisation", polygon_personalisation);

		return content;
	},

    /**
     * @param {polygon} polygon
     * @param {namespace} utils
     * @returns {string}
     * @private
     */
	_generate_polygon_user_content: function(polygon, utils) {
		// Update polygon_template
		var content = '';

		if (polygon._completed) {
			content = utils.generate_polygon_html(this.Constants.Templates.polygon_completed_template);
		}
		else {
			content = utils.generate_polygon_html(this.Constants.Templates.polygon_template);
		}
		
		var template_references = utils.get_polygon_references(content);
		
		for (var i in template_references) {
			content = utils.fill_reference(polygon, content, template_references[i], true);
		}

		return content;
	},

    /**
     * @param {polygon} polygon
     * @returns {string}
     * @private
     */
	_generate_polygon_content: function(polygon) {
		var polygon_id = polygon.id.replace("#polygon_id: ", "");
		
		var self = this;
		
		var local_utils = {
			polygon_id: polygon_id,
			
			generate_polygon_html: function(template) {
				return template.innerHTML.replace(/#id/g, polygon_id);
			},
			
			get_polygon_references: function(html) {
				var all_references = html.match(/\$.[^_" <\/]+/g);

				return all_references;
			},

			escaped_string: function(str) {
				return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
			},

			fill_reference: function(polygon, content, reference, full_path) {
				var property = reference.substring(1);

				var value = get_property(polygon, "_" + property);

				var is_value_non_empty = (((value != null) && (value != undefined)) &&
										  (typeof(value) != "string" || (value != "")));

				var re = new RegExp("_" + this.escaped_string(property), "g");

				if (full_path) {
					var name = property.replace("content.", "");
					
					switch(name) {
					case "image":
						{
							value = self.get_file('Asset', value);
							break;
						}
					case "html":
						{
							value = self.get_file('Other', value);
							break;
						}
					default:
						break;
					}
				}

				return content.replace(re,
									   is_value_non_empty ? value : "");
			},

			get_select_status: function (value, select_value) {
				if (value == select_value) {
					return 'selected';
				}

				return '';
			}
		};

		var content = null;
		
		if (this.config.edition) {
			content = this._generate_polygon_edition_content(polygon, local_utils);
		}
		else {
			content = this._generate_polygon_user_content(polygon, local_utils);
		}

		// TODO: Move this somewhere where it belongs.
		if (this._training_level || this.config.edition) {
			try {
				var info_pin = this.PSV.getMarker("#info_pin_id: " + polygon_id);

				if (polygon._infotype != "aucun") {
					info_pin.image = this.get_file('InfoType', this.Constants.InfoTypes[polygon._infotype]);
					info_pin.content = content;

					this.PSV.updateMarker(info_pin, false);
				}
				else {
					this.PSV.removeMarker(info_pin, false);
				}
			}
			catch(e) {
				if (polygon._infotype &&
					(polygon._infotype != "aucun")) {
					var bounding_box = polygon.bounding_box;
					
					var min_coord = bounding_box[0];
					var max_coord = bounding_box[1];

					// FIXME: Invalid bounding box when warping.
					if ((max_coord[0] - min_coord[0]) >= (this.PSV.prop.pano_data.cropped_width / 2)) {
						min_coord[0] += this.PSV.prop.pano_data.cropped_width;

						var temp = min_coord[0];
						min_coord[0] = max_coord[0];
						max_coord[0] = temp;
					}

					var width = (max_coord[0] - min_coord[0]);
					var height = (max_coord[1] - min_coord[1]);

					var pin_width = width / 5;
					var pin_height = height / 5;

					if (pin_width < 40) pin_width = 40;
					if (pin_height < 40) pin_height = 40;

					// Add info marker at center of bounding box.
					var info_pin_properties = {
						id: "#info_pin_id: " + polygon_id,
						x: (min_coord[0] + width / 2),
						y: (min_coord[1] + height / 2),
						width: pin_width,
						height: pin_height,
						image: this.get_file('InfoType', this.Constants.InfoTypes[polygon._infotype]),
						content: content,
						data: {
							deletable: true
						}
					};

					this.PSV.addMarker(info_pin_properties, false);
				}
			}
		}

		return content;
	},

	// TODO: Merge with add_polygon parts that are common to both.
    /**
     * @param {object} properties
     * @param {boolean} edition
     * @param {boolean} render
     * @returns {polygon}
     * @private
     */
    _load_polygon: function (properties, edition, render) {
		try {
			var polygon_properties = {
				id: "#polygon_id: " + this._polygon_id,
				_scene_id: this._scene_id,

				className: "polygon",

				svgStyle: {
					fill: 'url(#points)',
					stroke: 'rgba(255, 0, 50, 0.8)',
					'stroke-width': '2px'
				},
				
				tooltip: {
					content: 'Indéfini',
					position: 'right center'
				},

				data: {
					deletable: true,
					selectable: true
				}
			};

			for (var i in this.Constants.PolygonEditableFields) {
				polygon_properties["_" + this.Constants.PolygonEditableFields[i]] = "";
			}

			PhotoSphereViewer.Utils.deepmerge(polygon_properties, properties);
			polygon_properties.svgStyle.opacity = (edition || DEBUG_MODE || polygon_properties._completed) ? 1.0 : 0.0;
			polygon_properties.data.deletable = edition || DEBUG_MODE;

			if (!edition) {
				polygon_properties.tooltip = null;
			}

			// Compute bounding box
			var min_x = Number.MAX_VALUE;
			var max_x = Number.MIN_VALUE;
			var min_y = Number.MAX_VALUE;
			var max_y = Number.MIN_VALUE;

			for (i in polygon_properties.polygon_px) {
				var point = polygon_properties.polygon_px[i];

				min_x = Math.min(min_x, point[0]);
				max_x = Math.max(max_x, point[0]);
				
				min_y = Math.min(min_y, point[1]);
				max_y = Math.max(max_y, point[1]);
			}

			polygon_properties.bounding_box = [[min_x, min_y], [max_x, max_y]];

			var new_polygon = this.PSV.addMarker(polygon_properties, render);
			new_polygon.content = this._generate_polygon_content(new_polygon)

			++this._polygon_id;

			return new_polygon;
		}
		catch (e)
		{
			console.log(e);

			return null;
		}
    },

    /**
     * @param {polygon} polygon
     * @param {Object[]} answers
     * @param {number} score
     * @private
     */
	_set_polygon_completed: function(polygon, answers, score) {
		try {
			var index = parseInt(polygon.id.replace("#polygon_id: ", ""));
			var scene_index = this._all_scene_ids.indexOf(this._scene_id);

			var saved_polygon_data = this._all_scenes[scene_index].all_polygons[index];
			
			saved_polygon_data._completed = true;
			saved_polygon_data._score = score;

			if (answers) {
				saved_polygon_data._submittedAnswers = JSON.parse(JSON.stringify(answers));
			}

			PhotoSphereViewer.Utils.deepmerge(polygon, saved_polygon_data);
			
			polygon.svgStyle.opacity = 1;
			polygon.content = this._generate_polygon_content(polygon);

			this.PSV.updateMarker(polygon, true);
			
			var old_scroll = this.PSV.panel.content.scrollTop;
			
			this.PSV.showPanel(polygon.content);
			this.PSV.panel.content.scrollTop = old_scroll;
		}
		catch(e) {
			console.log(e);
		}
	},

    /**
     * @param {polygon} polygon
     * @param {Object[]} answers
     * @param {number} max_answer_count
     * @private
     */
	_set_quizz_answers: function(polygon, answers, max_answer_count) {
		var all_answers = answers.map(function(x) {return {valid: x}});
		
		var score = this._compute_polygon_score(polygon, all_answers, max_answer_count);
		
		polygon._completed = true;
		polygon._score = score;
		polygon._submittedAnswers = JSON.parse(JSON.stringify(all_answers));
	},

    /**
     * @param {polygon} polygon
     * @param {Object[]} all_answers
     * @param {number} max_answer_count
     * @returns {number}
     * @private
     */
	_compute_polygon_score: function(polygon, all_answers, max_answer_count) {
		var score = 0;
		var answer_count = 0;
		var wrong_count = 0;

		for (var i = 0; i < all_answers.length; ++i) {
			var actual_answer = polygon._content.quizz.answers[i];

			var answer_score = parseInt(actual_answer.coeff);
			
			if (all_answers[i].valid){
				++answer_count;
			}

			if ((actual_answer.coeff > 0) && all_answers[i].valid) {
				score += answer_score;
				all_answers[i].status = "label-success";
			}
			else if (!actual_answer.coeff && !all_answers[i].valid) {
				// Nothing.
			}
			else {
				if (actual_answer.coeff > 0) {
					all_answers[i].status = "label-warning";
				}
				else {
					if (all_answers[i].valid) {
						if (actual_answer.coeff < 0) {
							score += answer_score;
						}
						else {
							score -= Math.floor(100 / max_answer_count);
						}

						all_answers[i].status = "label-danger";
                        ++wrong_count;
					}
				}
			}
		}

		if (wrong_count &&
            (answer_count == max_answer_count)) {
			score = 0;
		}
		
		if (score < 0) {
			score = 0;
		}

        if (score > 100) {
            score = 100;
        }

		return score;
	},

	// TODO: Get from LMS. (And remove localStorage)
    /**
     * @param {string} json
     * @private
     */
	_resume_game: function(json) {
		if (!json) {
			return;
		}

		var data = JSON.parse(json);

		this._tracker.history = data.history;

		for(var i in data.polygons) {
			var polygon_data = data.polygons[i];
			var scene_index = this._all_scene_ids.indexOf(polygon_data.scene);
			
			var scene = this._all_scenes[scene_index];
			var polygon = scene.all_polygons[polygon_data.index];

			if (polygon_data.answers) {
				if (!polygon_data.answers.length) {
					continue;
				}
				
				var max_answer_count = Object.keys(polygon._content.quizz.answers).length;
				
				this._set_quizz_answers(polygon, polygon_data.answers, max_answer_count);
			}
			else if (polygon._completed) {
				polygon._completed = true;
			}
		}
	},

    /**
     * @param {string} scene_id
     * @returns {Object}
     * @private
     */
	_load_scene: function(scene_id) {
		this._scene_id = scene_id;

		this._polygon_id = 0;
		this._vertex_id = 0;
		this._polygon_vertices = [];
		
		if (this.config.edition) {
			var json = localStorage.getItem(this.level_id + "_" + scene_id);

			if (json == null) return null;
			
			var data = JSON.parse(json);

			return data;
		}
		else {
			var index = this._all_scene_ids.indexOf(scene_id);

			return this._all_scenes[index];
		}
	},

    /**
     * @param {string} scene_id
     * @param {string} panorama
     * @private
     */
	_append_scene_miniature: function(scene_id, panorama) {
		if ((panorama == null) ||
			(panorama == undefined)) {
			var json = localStorage.getItem(this.level_id + "_" + scene_id);

			if (json == null) return;

			var data = JSON.parse(json);

			panorama = data["panorama"];
		}

		var is_default_scene = (this._all_scene_ids.indexOf(scene_id) == 0);
		
		var scene_miniature = this.Constants.Templates.scene_miniature_template.innerHTML
			.replace(/#id/g, scene_id)
			.replace(/_panorama/g, this.get_file('Panorama', panorama))
			.replace(/#style/g, is_default_scene ? 'display: inline;' : 'display: none;');

		this._scene_selection_panel += scene_miniature;
	},

    /**
     * @function
     * @private
     * @fires level-complete
     * @fires partial-level-complete
     */
	_complete_level: function() {
		var visited_scenes = this._tracker.getVisitedScenes();

		var non_visited_scenes = this._all_scene_ids.filter(function(i) {
			return visited_scenes.indexOf(i) < 0;
		});

		if (!non_visited_scenes.length) {
			if (confirm("Êtes-vous sûr d'avoir terminé ?")) {
				this.PSV.trigger("level-complete");
			}
		}
		else if (confirm("Certaines scenes n'ont pas été visitées, êtes vous sûr de vouloir continuer ?")) {
			this.PSV.trigger("level-complete");
		}
		else {
			this.PSV.trigger("partial-level-complete");
		}
	},

    /**
     * @param {Object} properties
     * @private
     */
    _load_level_data: function (properties) {
		if (properties.edition) {
			var json = localStorage.getItem(properties.level_id);

			if (json == null) return;

			var data = JSON.parse(json);

			this._all_scene_ids = data.scenes;
			this._training_level = data.training;
		}
		else {
			this._load_compact_level(properties.from_json);
		}

		for(var i in this._all_scene_ids) {
			this._append_scene_miniature(this._all_scene_ids[i]);
		}
    },


    /**
     * @param {Object} properties
     * @return {Object}
     * @private
     */
    _init_play_mode: function (properties) {
		this._tracker = new Tracker(this);

		var self = this;
		
        // Navbar
        var navbar =
            [
                'zoom',
                'spacer-1',
				'spacer-1',
				{
                    title: "Terminer",
					content: '<i class="fa fa-check" aria-hidden="true"></i>',
					className: 'custom-button',
                    enabled: true,
                    onClick: self._complete_level.bind(self)
                },
                'caption',
                'gyroscope',
                'fullscreen'
            ];

		if (DEBUG_MODE) {
			navbar.splice(5, 0, 'markers');
		}

		return navbar;
    },

    /**
     * @param {Object} properties
     * @return {Object}
     * @private
     */
    _init_edition_mode: function (properties) {
        var self = this;

        // Navbar
        var navbar =
            [
                'autorotate', 'zoom',
				'spacer-1',
                {
					id: "save_level",
                    title: 'Sauvegarder le niveau',
                    className: 'custom-button',
                    content: '<i class="fa fa-floppy-o" aria-hidden="true"></i>',
                    onClick: self.save_level.bind(self)
                },
				'spacer-1',
				{
					id: "new_scene",
					title: 'Nouvelle scène',
					className: 'custom-button',
					content: '<i class="fa fa-plus" aria-hidden="true"></i>',
					onClick: self._input_new_scene.bind(self)
				},
				{
					id: "goto_scene",
					title: 'Aller à',
					className: 'custom-button',
					content: '<i class="fa fa-share" aria-hidden="true"></i>',
					onClick: self._show_scene_selection_panel.bind(self)
				},
				{
					id: "set_as_default",
					title: "Définir en tant que scène par défaut",
					className: 'custom-button',
					content: '<i class="fa fa-map-pin" aria-hidden="true"></i>',
					onClick: function () {
						if (!self._scene_id) {
							return;
						}
						
						self.PSV.getNavbarButton("set_as_default").disable();
						self._set_current_scene_as_default()
					}
				},
				'spacer-1',
				'markers'
            ];

		navbar.push('spacer-1');

		var all_tools = {
            marker: new MarkerTool(this),
            remove: new RemoveMarkerTool(this)
        };

		var tool_button_count = 0;
		
		var tool_buttons = [];
        for (var i in all_tools) {
            var tool = all_tools[i];
            var active_by_default = (this.current_tool == tool);

            var create_tool_button = function (tool) {
				var id = "button_tool_" + tool_button_count++;
				tool.button_id = id;

                return {
                    id: id,
                    title: tool.description,
					className: "custom-button",
                    content: tool.icon || i,
                    enabled: true,
                    onClick: (function () {
                        return function () {
							if (!self._scene_id) {
								return;
							}
							
                            if (tool != self.current_tool) {
                                if (self.current_tool) {
                                    self.current_tool.deselect()
									
									var container = self.PSV.getNavbarButton(self.current_tool.button_id).container;
									container.className = container.className.replace("button-selected", "");
                                }

                                self.current_tool = tool;
                                tool.select();

								var container = self.PSV.getNavbarButton(tool.button_id).container;
								container.className += " button-selected"
                            }
							else {
								tool.deselect();
								self.current_tool = null;

								var container = self.PSV.getNavbarButton(tool.button_id).container;
								container.className = container.className.replace("button-selected", "");
							}
                        }
                    }())
                };
            }

            tool_buttons.push(create_tool_button(tool));
        }
		
		tool_buttons.splice(1, 0, {
			title: 'Valider la zone',
			id: "button_validate_polygon",
			className: 'custom-button',
			content: '<i class="fa fa-check" aria-hidden="true"></i>',
			onClick: self.validate_current_polygon.bind(self),
			enabled: false
		});

		navbar = navbar.concat(tool_buttons);

        navbar.push('caption',
					'gyroscope',
					'fullscreen');

		return navbar;
    },

    /**
     * @param {string} polygon_id
     * @return {boolean}
     * @private
     */
    _update_polygon_content: function (polygon_id) {
        try {
			var polygon = this.PSV.getMarker("#polygon_id: " + polygon_id);

			var get_field_value = function(field, default_value) {
				var value = default_value;
				
				if (field == null) {
					value = default_value;
				}
				else if (field.type == 'checkbox') {
					value = field.checked;
				}
				else if (field.type == 'file') {
					value = field.value.substring(field.value.lastIndexOf("\\") + 1);
				}
				else {
					value = field.value;
				}
				
				if (typeof(value) == "string") {
					value = value.trim();
				}

				return value;
			}

			var update_references = function(index) {
				var at_least_one = false;
				
				for (var i in all_references) {
					var reference = all_references[i].substring(1);

					if (index != -1) {
						reference = reference
							.replace(/#count/g, index)
							.replace(/#ncount/g, index + 1);
					}

					var field = document.getElementById("$" + reference + "_" + polygon_id);

					if (field != null) {
						var value = get_field_value(field, get_property(polygon, '_' + reference));

						// TODO?: Do this for other streaming sites?
						if (value && reference.endsWith('.url')) {
							if (value.match(/youtube\.com/)) {
								value = value.replace("watch?v=", "embed/");
							}
						}
                        else if (value && reference.endsWith('.pdf')) {
                            if (value.match("/drive\.google\.com/")) {
                                value = value.replace("/view", "/preview");
                            }
                        }

						if ((typeof(value) == "boolean") || value) {
							set_property(polygon, "_" + reference, value);

							if (field.type != "checkbox") {
								at_least_one = true;
							}
						}
					}
				}

				return at_least_one;
			}

			var all_references = this.Constants.Templates.polygon_template.innerHTML.match(/\$.[^_" <\/]+/g);

			for (var i in all_references) {
				all_references[i] = all_references[i].replace(/#id/g, polygon_id);
			}

			update_references(-1);

			all_references = this.Constants.Templates.polygon_personalisation_template.innerHTML.match(/\$.[^_" <\/]+/g);

			for (i in all_references) {
				all_references[i] = all_references[i].replace(/#id/g, polygon_id);
			}

			// Update already present ones and new one.
			var count = 0;
			for (; count <= polygon._content.length; ++count) {
				update_references(count);
			}

			// Update select inputs.
			try {
				var select_list = document.getElementById("polygon_content_" + polygon_id).querySelectorAll("select");

				for (i = 0; i < select_list.length; ++i) {
					var selection = select_list[i];
					var property = selection.id.match("polygon_([^ _]+)")[1].replace(/-/g, "_");
					var value = get_field_value(selection)
					
					set_property(polygon, "_" + property, value);					
				}
			} catch (e) {
			}

            polygon.tooltip.content = polygon._name;
            polygon.content = this._generate_polygon_content(polygon);
			
			this.PSV.panel.content.innerHTML = polygon.content;
            this.PSV.updateMarker(polygon);
        }
        catch (e) {
            console.log(e);
        }

		return false;
    },

    /**
     * @function
     * @private
     */
	_input_new_scene: function() {
		var scene_template = document.getElementById("scene_template");
		this.PSV.showPanel(scene_template.innerHTML);
	},

    /**
     * @function
     * @private
     */
	_save_current_scene: function () {
        var polygon_data = [];

		var data_to_save = ["polygon_px", "width", "height",
							"longitude", "latitude",
							"className", "anchor", "visible",
							"tooltip", "style", "_name",
							"_scene_id", "_content"];

		for (var i in this.Constants.PolygonEditableFields) {
			data_to_save.push("_" + this.Constants.PolygonEditableFields[i]);
		}

        for (i = 0; i < this._polygon_id; i++) {
            try {
                var polygon = this.PSV.getMarker("#polygon_id: " + i);

                var usefull_data = {};

                for (var field_num in data_to_save) {
                    var field = data_to_save[field_num];
                    var field_data = polygon[field];

                    if (field_data != undefined) {
                        usefull_data[field] = field_data;
                    }
                }

                polygon_data.push(usefull_data);
            }
            catch (e) {
                console.log(e);
            }
        }

		var panorama = this.PSV.config.panorama;
		
		var data = {
			panorama: panorama.substring(panorama.lastIndexOf("/") + 1),
			all_polygons: polygon_data
		};

        var json = JSON.stringify(data);

        localStorage.setItem(this.level_id + "_" + this._scene_id, json);
	},

    /**
     * @function
     * @private
     */
	_show_scene_selection_panel: function() {
		this.PSV.showPanel(this._scene_selection_panel);

		var current_scene_miniature = document.getElementById("scene_miniature_content_" + this._scene_id);

		if (current_scene_miniature != null) {
			current_scene_miniature.style.display = 'none';
		}

		for (var i in this._all_scene_ids) {
			var scene_id = this._all_scene_ids[i];
			
			if (scene_id == this._scene_id) {
				continue;
			}
			
			var star = document.getElementById("default_scene_" + scene_id);

			if (star != null) {
				star.style.display = (i == 0) ? 'inline' : 'none';
			}
		}
	},

    /**
     * @function
     * @returns {Object}
     * @private
     */
	_get_level_default_scene: function() {
		if (!this.config.edition) {
			if (this._tracker.history.length) {
				var last_record = this._tracker.history[this._tracker.history.length - 1];
				
				return last_record.scene;
			}
		}
		
		if (this._all_scene_ids.length > 0) {
			return this._all_scene_ids[0];
		}

		return null;
	},

    /**
     * @function
     * @private
     */
	_start_level: function() {
		var default_scene = this._get_level_default_scene();

		if (default_scene != null) {
			this.change_scene_to(default_scene);
		}
	},

    /**
     * @function
     * @private
     */
	_save_compact_level: function() {
		var data = {
			all_scenes: [],
			all_polygons: [],
			training: this._training_level
		};
		
		for (var i in this._all_scene_ids) {
			var scene_id = this._all_scene_ids[i];
			var scene_data = JSON.parse(localStorage.getItem(this.level_id + "_"  + scene_id));

			data.all_scenes.push({
				id: scene_id,
				panorama: scene_data.panorama
			});

			data.all_polygons = data.all_polygons.concat(scene_data.all_polygons);
		}

		var concat = function(x, y) {
			return x.concat(y);
		}
		
		var flatmap = function(arr, lambda) {
			return arr.map(lambda).reduce(concat, [])
		}

		var exist_and_uniq = function(x, pos, self) {
			return (x != undefined) && (self.indexOf(x) == pos);
		}

		var self = this;
		var all_resources = {
			panoramas: data.all_scenes.map(function(x) {return x.panorama;})
				.filter(exist_and_uniq),
			icons: data.all_polygons
				.map(function(x) {
					return self.Constants.InfoTypes[x._infotype];
				})
				.filter(exist_and_uniq),
			assets: flatmap(data.all_polygons,
							function(x) {
								return x._content.map(function(y) {
									return y.image;
								})
							}).filter(exist_and_uniq),
			others: flatmap(data.all_polygons,
							function(x) {
								return flatmap(x._content, function(y) {
									var all_files = [];
									var all_others = ["pdf", "html"];
									
									for (var i in all_others) {
										var file = y[all_others[i]];

										if (file) {
											all_files.push(file);
										}
									}
									
									return all_files;
								})
							}).filter(exist_and_uniq)
		}

		localStorage.setItem("resources", JSON.stringify(all_resources));
		localStorage.setItem("compact_" + this.level_id, JSON.stringify(data))
	},

    /**
     * @param {boolean}
     * @private
     */
	_load_compact_level: function(from_json_file) {
		var json = null;

		if (from_json_file) {
			var xobj = new XMLHttpRequest();
			
			xobj.overrideMimeType("application/json");
			xobj.open('GET', this.get_file('Level', this.level_id  + '.json'), false);
			xobj.onreadystatechange = function () {
				if (xobj.readyState == 4 && xobj.status == "200") {
					json = xobj.responseText;
				}
			};
			
			xobj.send(null);
		}
		else {
			json = localStorage.getItem("compact_" + this.level_id);
		}

		if (json == null) return;

		var data = JSON.parse(json);

		this._all_scenes = [];
		this._all_scene_ids = [];

		for (var i in data.all_scenes) {
			var scene = data.all_scenes[i];
			this._all_scene_ids.push(scene.id);
			
			this._all_scenes.push({
				panorama: scene.panorama,
				all_polygons: []
			})
		}

		var suspended_json = localStorage.getItem("suspend_data");
		// localStorage.removeItem("suspend_data");

		var suspended_data = suspended_json ? JSON.parse(suspended_json) : null;
		var polygons_with_content = 0;

		for (i in data.all_polygons) {
			var polygon = data.all_polygons[i];
			var index = this._all_scene_ids.indexOf(polygon._scene_id);

			var scene = this._all_scenes[index];

			if (!this.config.edition &&
				polygon._content.length) {

				var content_index = null;
				
				if (suspended_data) {
					var suspended_polygon = suspended_data.polygons[polygons_with_content];
					content_index = suspended_polygon.content_index;
				}
				else {
					content_index = Math.floor((Math.random() * polygon._content.length));
				}
				
				polygon._content = polygon._content[content_index];
				polygon._content_index = content_index;
		
				if (get_property(polygon, "_content.quizz.question")) {
					++this._total_question_count;
					this._total_question_coeff += parseInt(polygon._coeff);

					if (isNaN(this._scene_question_count[polygon._scene_id])) {
						this._scene_question_count[polygon._scene_id] = 1;
						this._scene_question_coeff[polygon._scene_id] = parseInt(polygon._coeff);
					} 
					else {
						++this._scene_question_count[polygon._scene_id];
						this._scene_question_coeff[polygon._scene_id] += parseInt(polygon._coeff);
					}
				}
				++polygons_with_content;
			}

			scene.all_polygons.push(polygon);
		}

		this._resume_game(suspended_json);
		
		this._training_level = data.training;
	},

    /**
     * @function
     * @private
     */
	_set_current_scene_as_default: function() {
		var current_index = this._all_scene_ids.indexOf(this._scene_id);

		if (current_index > 0) {
			var old_default_scene = this._all_scene_ids[0];
			
			this._all_scene_ids.splice(current_index, 1);
			this._all_scene_ids.splice(0, 0, this._scene_id);

			var old_star = document.getElementById("default_scene_" + old_default_scene);
			if (old_star != null) {
				old_star.style.display = 'none';
			}
		}
	}
};



window.onbeforeunload = function () {
    return 'Êtes-vous sûr de vouloir quitter ?';
};
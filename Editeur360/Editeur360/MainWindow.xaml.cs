﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Sockets;

namespace Editeur360
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// used to launch Chrome with Selenium (cf. Selenium documentation) 
        /// when we click on Edit Button. 
        /// </summary>
        RemoteWebDriver driver = null;
        /// <summary>
        /// Name of the selected or created level.
        /// </summary>
        string selectedLevel = null;
        /// <summary>
        /// Name of the new files that we want import.
        /// </summary>
        string newFileName = null;
        /// <summary>
        /// Path of the file to import.
        /// </summary>
        string fileToImport = null;
        /// <summary>
        /// Absolute path to the root of application.
        /// </summary>
        string root = AppDomain.CurrentDomain.BaseDirectory;
        /// <summary>
        /// Relativ path started at root.
        /// </summary>
        string tempRelativPath = Path.Combine("resources", "temp");
        string levelsRelativPath = Path.Combine("resources", "levels");
        string assetsRelativPath = Path.Combine("resources", "assets");
        string scormRelativPath = Path.Combine("resources", "sco_merieux");
        string scormAssetsRelativPath = Path.Combine("shared", "assets");
        /// <summary>
        /// Actual view Name.
        /// </summary>
        string actualView = null;
        /// <summary>
        /// Precedent view Name.
        /// </summary>
        string precedentView = null;

        /// <summary>
        /// Generat mainwindows of the application. Set it to initial view and define the icon.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            var pathToIcon = Path.Combine(root, assetsRelativPath, "360.ico");
            Uri iconUri = new Uri(pathToIcon, UriKind.RelativeOrAbsolute);
            this.Icon = BitmapFrame.Create(iconUri);
            ClearDriver();
            PromptView("initial");
            this.Closing += (e, f) => { try { driver.Quit(); } catch { } finally { driver = null; } };
        }


        /// <summary>
        /// close the driver and set it to null.
        /// </summary>           
        private void ClearDriver()
        {
            if (driver != null)
            {
                try
                {
                    driver.Close();
                    driver.Quit();
                }
                catch
                {
                }
                finally
                {
                    driver = null;
                }
            }
        }

        /// <summary>
        /// return the content of localstorage of the browser
        /// </summary>
        /// <returns>string localStorageContent </returns>
        private string GetLocalStorage()
        {
            if (driver != null)
            {
                try
                {
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

                    string localStorageLengthScript = "return localStorage.length;";
                    String localStorageLength = js.ExecuteScript(localStorageLengthScript).ToString();
                    int lenLocalStorage = 0;
                    Int32.TryParse(localStorageLength, out lenLocalStorage);
                    String localStorageContent = "{";
                    for (int i = 0; i < lenLocalStorage; i++)
                    {
                        if (i != 0)
                        {
                            localStorageContent += ",";
                        }
                        string localStorageKeyIScript = "return localStorage.key(" + i + ");";
                        String localStorageKeyI = js.ExecuteScript(localStorageKeyIScript).ToString();
                        string localStorageValueIScript = "return localStorage[\"" + localStorageKeyI + "\"];";
                        String localStorageValueI = js.ExecuteScript(localStorageValueIScript).ToString();
                        localStorageContent += "\"" + localStorageKeyI + "\"" + ":" + localStorageValueI;
                    }
                    localStorageContent += "}";
                    Console.WriteLine("content : " + localStorageContent);
                    return localStorageContent;
                }
                catch
                {
                    System.Windows.MessageBox.Show("Vous avez fermez la fenêtre Chrome avant de sauvergarder. Vos modifications n'ont pas pu être sauvées. Veuillez recommencer.");
                    return "ChromeClosed";
                }

            }
            else { return ""; }
        }

        /// <summary>
        /// copy and rename (if rename=true) a file 
        /// </summary>
        /// <param name="destPath">path to the dest where we store the file</param>
        /// <param name="rename">true = have to rename the file to copy; false = do not rename it</param>
        /// <returns>bool : false if user choose not to delete the existant file and he want rename it.</returns>
        private bool CopyAndRename(string destPath, bool rename)
        {
            string newDest = Path.Combine(destPath, newFileName);

            if (!File.Exists(newDest))
            {
                File.Copy(fileToImport, newDest, true);
            }
            else
            {
                MessageBoxResult wantReplaceFile = System.Windows.MessageBox.Show("Un fichier existe déjà sous ce nom. Etes-vous sûr(e) de vouloir écraser ce fichier ?", "Confirmer l'écrasement", MessageBoxButton.YesNo);
                if (wantReplaceFile == MessageBoxResult.Yes)
                {
                    Console.WriteLine(newDest);
                    File.Copy(fileToImport, newDest, true);
                }
                else if (wantReplaceFile == MessageBoxResult.No && rename)
                {
                    System.Windows.MessageBox.Show("Veuillez choisir un autre nom pour le fichier.");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// change the value of the boolean training in the json of the level
        /// </summary>
        /// <param name="isTraining"> new value for setting the training on the json.</param>
        private void SetLevelTraining(bool isTraining)
        {
            string pathToSelectedLevel = Path.Combine(root, levelsRelativPath, selectedLevel);
            string levelData = Path.Combine(root, levelsRelativPath, selectedLevel, selectedLevel + ".json");

            if (File.Exists(levelData))
            {
                string jsonContent = null;
                using (StreamReader r = new StreamReader(levelData))
                {
                    jsonContent = r.ReadToEnd();
                    jsonContent = jsonContent.Replace("\"training\":" + (!isTraining).ToString().ToLower(), "\"training\":" + isTraining.ToString().ToLower());
                }
                using (StreamWriter w = new StreamWriter(levelData, false))
                {
                    w.Write(jsonContent);
                }
            }
        }

        /// <summary>
        /// copy a directory
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs">true = copy the sub directory</param>
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        /// <summary>
        /// Returns the string corresponding to the objectives to be put in the imsmanifest for the scorm.
        /// </summary>
        /// <param name="jsonContent">content of the json of the level</param>
        /// <returns>The string corresponding to the objectives to be put in the imsmanifest for the scorm.</returns>
        private string FillObjectives(string jsonContent)
        {
            string objectives = "";
            string objective = "<imsss:objective objectiveID = \"#toReplace\" ></imsss:objective>";
            dynamic json = JsonConvert.DeserializeObject(jsonContent);
            dynamic compact = json["compact_demo"];
            dynamic all_scenes = compact["all_scenes"];
            dynamic all_polygons = compact["all_polygons"];
            Dictionary<string, int> number_of_polygon_by_scene = new Dictionary<string, int>();
            foreach (var i in all_scenes)
            {
                string scene_id = i["id"];
                objectives += objective.Replace("#toReplace", scene_id.Replace(" ", "_")) + "\n";
                number_of_polygon_by_scene[scene_id] = 0;
                Console.WriteLine("objectives :" + objectives);
            }
            foreach (var i in all_polygons)
            {
                string polygon_name = i["_name"];
                string polygon_scene_id = i["_scene_id"];
                int polygon_id = number_of_polygon_by_scene[polygon_scene_id];
                objectives += objective.Replace("#toReplace", polygon_scene_id.Replace(" ", "_") + "_" + polygon_name.Replace(" ", "_") + "_" + polygon_id) + "\n";
                number_of_polygon_by_scene[polygon_scene_id] += 1;
                Console.WriteLine("objectives :" + objectives);
            }
            return objectives;
        }

        /// <summary>
        /// Returns the string corresponding to the files to be put in the imsmanifest for the scorm.
        /// </summary>
        /// <param name="shared_assets">String corresponding to the files to be put in the imsmanifest for the scorm.</param>
        /// <param name="list_assets">List of files to put in the imsmanifest for the scorm.</param>
        /// <param name="tempScorm">Path to the folder where the scorm package is temporarily stored with being zipped.</param>
        /// <param name="assets_type">assets, panoramas, others, icons</param>
        /// <returns>shared_assets</returns>
        private string FillSharedAssets(string shared_assets, dynamic list_assets, string tempScorm, string assets_type)
        {

            string assets_path = Path.Combine(root, assetsRelativPath);
            string icons_path = Path.Combine(assets_path, "icons");
            string scorm_assets_path = Path.Combine(tempScorm, scormAssetsRelativPath);
            string scorm_panoramas_path = Path.Combine(tempScorm, scormAssetsRelativPath, "panoramas");
            string scorm_icons_path = Path.Combine(tempScorm, scormAssetsRelativPath, "icons");
            string scorm_others_path = Path.Combine(tempScorm, scormAssetsRelativPath, "others");

            string assetsPath = null;
            string assetsDest = null;
            switch (assets_type)
            {
                case "panoramas":
                    assetsPath = assets_path;
                    assetsDest = scorm_panoramas_path;
                    break;
                case "others":
                    assetsPath = assets_path;
                    assetsDest = scorm_others_path;
                    break;
                case "icons":
                    assetsPath = icons_path;
                    assetsDest = scorm_icons_path;
                    break;
                default:
                    assetsPath = assets_path;
                    assetsDest = scorm_assets_path;
                    break;
            }
            Console.WriteLine(list_assets);
            if (list_assets != null)
            {
                foreach (string asset in list_assets)
                {
                    string shared_asset = "<file href=\"shared/assets/#asset\"/>";
                    if (assets_type == "others" || assets_type == "icons" || assets_type == "panoramas")
                    {
                        shared_assets += shared_asset.Replace("#asset", assets_type + "/" + asset.Replace(" ", "_")) + "\n";
                    }
                    else
                    {
                        shared_assets += shared_asset.Replace("#asset", asset.Replace(" ", "_")) + "\n";
                    }
                    var assetPath = Path.Combine(assetsPath, asset);
                    var assetDest = Path.Combine(assetsDest, asset);
                    System.IO.File.Copy(assetPath, assetDest);
                    Console.WriteLine(asset);
                }
            }
            return shared_assets;
        }

        /// <summary>
        /// Copies the assets to the correct folders according to their type at the temporary scorm package level.
        /// </summary>
        /// <param name="jsonContent">Content of the json of the level</param>
        /// <param name="tempScorm">Path to the folder where the scorm package is temporarily stored with being zipped.</param>
        /// <returns>shared_assets = String corresponding to the files to be put in the imsmanifest for the scorm.</returns>
        private string CopyAndFillAssets(string jsonContent, string tempScorm)
        {
            string shared_assets = "";

            dynamic json = JsonConvert.DeserializeObject(jsonContent);
            dynamic resources = json["resources"];
            var list_assets = resources["assets"];
            var list_icons = resources["icons"];
            var list_others = resources["others"];
            var list_panoramas = resources["panoramas"];
            Console.WriteLine(list_icons.GetType());
            Console.WriteLine(list_assets.GetType());

            shared_assets = FillSharedAssets(shared_assets, list_assets, tempScorm, "assets");
            shared_assets = FillSharedAssets(shared_assets, list_icons, tempScorm, "icons");
            shared_assets = FillSharedAssets(shared_assets, list_others, tempScorm, "others");
            shared_assets = FillSharedAssets(shared_assets, list_panoramas, tempScorm, "panoramas");

            return shared_assets;
        }

        /// <summary>
        /// update precedentView and actualView with newView
        /// </summary>
        /// <param name="newView"></param>
        private void ChangeView(string newView)
        {
            precedentView = actualView;
            actualView = newView;
        }

        /// <summary>
        /// route to the proper display in View
        /// </summary>
        /// <param name="View">Name of the view to display.</param>
        private void PromptView(string View)
        {
            switch (View)
            {
                case "initial":
                    ChangeView(View);
                    PromptInitialView();
                    break;
                case "edit":
                    ChangeView(View);
                    PromptEditLevelView();
                    break;
                case "select":
                    ChangeView(View);
                    PromptSelectedLevelView();
                    break;
                case "name":
                    ChangeView(View);
                    PromptNewNameView("Name");
                    break;
                case "level":
                    ChangeView(View);
                    PromptNewNameView("Level");
                    break;
                default:
                    ChangeView("initial");
                    PromptInitialView();
                    break;
            }
        }

        /// <summary>
        /// display initial view
        /// </summary>
        private void PromptInitialView()
        {
            ClearDriver();

            SaveButton.IsEnabled = false;
            ExportButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
            EditButton.IsEnabled = false;

            AppDescription.Visibility = Visibility.Visible;
            LevelSelectionPanel.Visibility = Visibility.Hidden;
            NewNameDialogue.Visibility = Visibility.Hidden;
            NewLevelButtons.Visibility = Visibility.Hidden;
            NewNameButtons.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Displays the choice view of a new name for a file if type = "Name"
        /// Displays the choice view of a new name for a level if type = "Level"
        /// </summary>
        /// <param name="type">à pour valeur "Level" ou "Name"</param>
        private void PromptNewNameView(string type)
        {
            SaveButton.IsEnabled = false;
            ExportButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
            EditButton.IsEnabled = false;

            AppDescription.Visibility = Visibility.Hidden;
            LevelSelectionPanel.Visibility = Visibility.Hidden;
            NewNameDialogue.Visibility = Visibility.Visible;

            if (type.Equals("Level"))
            {
                NewLevelButtons.Visibility = Visibility.Visible;
                NewNameButtons.Visibility = Visibility.Hidden;
                LabelQuestion.Text = "Entrez le titre de votre nouveau niveau : ";
            }
            else if (type.Equals("Name"))
            {
                NewLevelButtons.Visibility = Visibility.Hidden;
                NewNameButtons.Visibility = Visibility.Visible;
                LabelQuestion.Text = "Entrez le nom que vous souhaitez attribuer à ce fichier : ";
            }
            else
            {
                PromptView("initial");
            }
        }

        /// <summary>
        /// Displays the level view you choose when you open a level or create a new one.
        /// </summary>
        private void PromptSelectedLevelView()
        {
            LevelDescription.Text = "Vous avez sélectionné le niveau : " + selectedLevel + "\n\nVous pouvez maintenant l'exporter, le supprimer ou l'éditer. \n\nN'oubliez pas d'importer vos panoramas/images/... s'ils n'ont jamais encore été utilisés.\n\n";
            ClearDriver();

            string pathToSelectedLevel = Path.Combine(root, levelsRelativPath, selectedLevel);
            string levelData = Path.Combine(root, levelsRelativPath, selectedLevel, selectedLevel + ".json");
            bool isTraining = true;

            if (File.Exists(levelData))
            {
                using (StreamReader r = new StreamReader(levelData))
                {
                    string jsonContent = r.ReadToEnd();
                    isTraining = !jsonContent.Contains("\"training\":false");
                }
            }

            TrainingRadioButton.IsChecked = isTraining;
            ExamRadioButton.IsChecked = !isTraining;
            Console.WriteLine("Training : " + isTraining);

            SaveButton.IsEnabled = false;
            ExportButton.IsEnabled = true;
            DeleteButton.IsEnabled = true;
            EditButton.IsEnabled = true;

            AppDescription.Visibility = Visibility.Hidden;
            LevelSelectionPanel.Visibility = Visibility.Visible;
            TrainingRadioButton.Visibility = Visibility.Visible;
            ExamRadioButton.Visibility = Visibility.Visible;
            NewNameDialogue.Visibility = Visibility.Hidden;
            NewLevelButtons.Visibility = Visibility.Hidden;
            NewNameButtons.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Prompt the view when we edit a level
        /// </summary>
        private void PromptEditLevelView()
        {
            LevelDescription.Text = "Vous êtes en train d'éditer le niveau : " + selectedLevel + "\n\n Une fenêtre Chrome a été ouverte pour vous permettre d'éditer vos niveaux. \n\n Ne fermez pas l'éditeur sous Chrome avant d'avoir sauvegardé vos modifications via Fichier > Sauvegarder. \n\n Pour exporter un niveau, sauvegardez d'abord le niveau. \n\n Pour supprimer un niveau, fermez d'abord l'éditeur sous Chrome.";

            SaveButton.IsEnabled = true;
            ExportButton.IsEnabled = true;
            DeleteButton.IsEnabled = false;
            EditButton.IsEnabled = false;

            AppDescription.Visibility = Visibility.Hidden;
            LevelSelectionPanel.Visibility = Visibility.Visible;
            TrainingRadioButton.Visibility = Visibility.Hidden;
            ExamRadioButton.Visibility = Visibility.Hidden;
            NewNameDialogue.Visibility = Visibility.Hidden;
            NewLevelButtons.Visibility = Visibility.Hidden;
            NewNameButtons.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Action made when clicking on the 'new' button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewLevelButton_Click(object sender, RoutedEventArgs e)
        {
            PromptView("level");
        }

        /// <summary>
        /// Action made when clicking on 'ok' of the view of a new level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewLevelOkDialogButton_Click(object sender, RoutedEventArgs e)
        {
            if (NewNameChoice.Text != null && NewNameChoice.Text != string.Empty)
            {
                selectedLevel = NewNameChoice.Text;
                Boolean levelNameChosen = false;
                string levelPath = Path.Combine(root, levelsRelativPath, selectedLevel);
                if (!Directory.Exists(levelPath))
                {
                    Directory.CreateDirectory(levelPath);
                    levelNameChosen = true;
                }
                else
                {
                    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Un niveau existe déjà sous ce nom. Etes-vous sûr(e) de vouloir écraser la précédente version?", "Confirmer l'écrasement", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        Console.WriteLine(levelPath);
                        Directory.Delete(levelPath, true);
                        Directory.CreateDirectory(levelPath);
                        levelNameChosen = Directory.Exists(levelPath);

                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Veuillez choisir un autre nom pour le niveau.");
                    }
                }
                if (levelNameChosen)
                {
                    string levelData = Path.Combine(levelPath, selectedLevel + ".json");
                    string defaultContent = "{\"demo\":{\"scenes\":[],\"training\":true}}";
                    bool needDefault = true;
                    if (File.Exists(levelData))
                    {
                        using (StreamReader r = new StreamReader(levelData))
                        {
                            string jsonContent = r.ReadToEnd();
                            if (jsonContent != String.Empty)
                            {
                                needDefault = false;
                            }
                        }
                    }
                    if (needDefault)
                    {
                        if (!Directory.Exists(levelPath)) { Directory.CreateDirectory(levelPath); }
                        using (StreamWriter w = new StreamWriter(levelData, false))
                        {
                            w.WriteLine(defaultContent);
                        }
                    }
                    PromptView("select");
                }
                else
                {
                    PromptView("level");
                }
            }
        }


        /// <summary>
        /// Action performed when clicking on 'cancel' the view of a new level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewLevelCancelDialogButton_Click(object sender, RoutedEventArgs e)
        {
            NewNameChoice.Text = "";
            PromptView(precedentView);
        }

        /// <summary>
        /// Action when clicking on 'open'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseLevelButton_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = Path.Combine(root, levelsRelativPath);
                DialogResult result = fbd.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string levelPath = fbd.SelectedPath;
                    Console.WriteLine(levelPath);
                    selectedLevel = Path.GetFileName(levelPath);
                    PromptView("select");
                }
            }
        }

        /// <summary>
        /// Action performed when clicking on 'save'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>                      
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (driver != null)
            {
                string localStorageContent = GetLocalStorage();
                if (localStorageContent != "{}" && localStorageContent != "" && localStorageContent != "ChromeClosed")
                {
                    Console.WriteLine("localstorage:" + localStorageContent);
                    string filePath = Path.Combine(root, levelsRelativPath, selectedLevel, selectedLevel + ".json");
                    try
                    {
                        StreamWriter file = new StreamWriter(filePath, false);
                        if (file != null)
                        {
                            file.WriteLine(localStorageContent);
                            file.Close();
                            System.Windows.MessageBox.Show("Sauvegarde effectuée");
                        }
                        else { System.Windows.MessageBox.Show("Erreur lors de la sauvegarde du niveau. Le fichier de sauvegarde n'a pas pu être créé correctement."); }
                    }
                    catch { System.Windows.MessageBox.Show("Erreur lors de la sauvegarde du niveau. StreamWriterException."); }
                }
                else if (localStorageContent.Equals("ChromeClosed")) { PromptView("initial"); }
                else { System.Windows.MessageBox.Show("Editez le niveau avant de l'exporter."); }
            }
            else { System.Windows.MessageBox.Show("Une erreur s'est produite lors de la tentative de sauvegarde des modification apportées via l'éditeur sous Chrome. \n\n Il se peut que vous ayez fermer votre fenêtre avant de sauvegarder, vous devrez alors recommencer vos modifications. \n\n Il se peut également que vous n'ayez pas modifié la scène, aucunes modifications n'est à sauvegarder."); }
        }

        /// <summary>
        /// Action when clicking on 'export'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            if (selectedLevel != null)
            {
                string zipDest = null;
                using (var fbd = new FolderBrowserDialog())
                {
                    fbd.SelectedPath = Path.Combine(root, levelsRelativPath);
                    DialogResult result = fbd.ShowDialog();

                    if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        zipDest = fbd.SelectedPath;
                        Console.WriteLine(zipDest);
                    }
                }
                if (zipDest != null && zipDest != string.Empty)
                {
                    var levelData = Path.Combine(root, levelsRelativPath, selectedLevel, selectedLevel + ".json");
                    if (File.Exists(levelData))
                    {
                        using (StreamReader jr = new StreamReader(levelData))
                        {
                            string jsonContent = jr.ReadToEnd();
                            if (jsonContent != null && jsonContent != string.Empty)
                            {
                                string objectives = FillObjectives(jsonContent);
                                var tempScorm = Path.Combine(root, tempRelativPath, selectedLevel + "_scorm_package");
                                if (Directory.Exists(tempScorm)) { Directory.Delete(tempScorm, true); }
                                DirectoryCopy(Path.Combine(root, scormRelativPath), tempScorm, true);

                                string demoJson = Path.Combine(tempScorm, "shared", "demo" + ".json");
                                dynamic json = JsonConvert.DeserializeObject(jsonContent);
                                var demoJsonContent = json["compact_demo"];
                                using (StreamWriter mw = new StreamWriter(demoJson))
                                { mw.Write(demoJsonContent); }


                                string imsmanifestReadPath = Path.Combine(root, scormRelativPath, "imsmanifest.xml");
                                string imsmanifestWritablePath = Path.Combine(tempScorm, "imsmanifest.xml");
                                using (StreamReader mr = new StreamReader(imsmanifestReadPath))
                                {
                                    string imsmanifestContent = mr.ReadToEnd();
                                    try
                                    {
                                        string shared_assets = CopyAndFillAssets(jsonContent, tempScorm);
                                        imsmanifestContent = imsmanifestContent.Replace("#shared_assets", shared_assets);
                                        imsmanifestContent = imsmanifestContent.Replace("#objectives", objectives);
                                        imsmanifestContent = imsmanifestContent.Replace("#name_level", selectedLevel);
                                        using (StreamWriter mw = new StreamWriter(imsmanifestWritablePath))
                                        { mw.Write(imsmanifestContent); }
                                        string zipFile = Path.Combine(zipDest, selectedLevel + ".zip");
                                        if (File.Exists(zipFile))
                                        {
                                            File.Delete(zipFile);
                                        }
                                        ZipFile.CreateFromDirectory(tempScorm, zipFile);
                                        Directory.Delete(tempScorm, true);
                                        System.Windows.MessageBox.Show("Export terminé.");

                                    }
                                    catch { System.Windows.MessageBox.Show("Problème lors de l'export du niveau, vous avez peut-être selectionné un niveau vide ou les données sont erronées. Veuillez choisir un autre niveau."); }
                                }
                            }
                            else { System.Windows.MessageBox.Show("La sauvegarde du niveau est vide"); }
                        }
                    }
                    else { System.Windows.MessageBox.Show("Problème lors de l'export du niveau, vous avez peut-être selectionné un niveau vide ou les données sont erronées. Veuillez choisir un autre niveau."); }
                }
                else { System.Windows.MessageBox.Show("Vous n'avez pas selectionné une destination valide ou vous avez annulé"); }

            }
            else { System.Windows.MessageBox.Show("Choisissez un niveau avant de l'exporter."); }
        }

        /// <summary>
        /// Action when clicking on 'delet'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            string levelPath = Path.Combine(root, levelsRelativPath, selectedLevel);
            if (Directory.Exists(levelPath))
            {
                Directory.Delete(levelPath, true);
                System.Windows.MessageBox.Show("Le niveau a bien été supprimé.");
            }
            else
            {
                System.Windows.MessageBox.Show("Le niveau que vous vouliez supprimer n'existe pas. \n\nVeuillez sélectionner un niveau existant.");
            }

            PromptView("initial");

        }

        /// <summary>
        /// Action when clicking on 'quit'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Action when clicking on 'edit'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            ClearDriver();
            var isConnectionAvailable = false;
            TcpClient client = new TcpClient();
            try
            {
                client.Connect("127.0.0.1", 8887);
                isConnectionAvailable = true;
                Console.WriteLine("Connection open, host active");
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Connection could not be established due to: \n" + ex.Message);
                System.Windows.MessageBox.Show("Veuillez lancer l'application Web Server afin d'éditer le niveau.");
            }
            finally
            {
                client.Close();
            }

            if (driver == null && isConnectionAvailable)
            {
                System.Windows.MessageBox.Show("L'éditeur va s'ouvrir dans une fenêtre Chrome.");

                var driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(driverService);
                string levelData = Path.Combine(root, levelsRelativPath, selectedLevel, selectedLevel + ".json");
                Console.WriteLine("path to json : " + levelData);
                driver.Navigate().GoToUrl("http://127.0.0.1:8887/index.html");
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                string clearLocalStorage = "localStorage.clear();";
                js.ExecuteScript(clearLocalStorage);
                if (File.Exists(levelData))
                {
                    string scriptSetLocalStorage = "var xobj = new XMLHttpRequest(); var json = null; xobj.overrideMimeType('application/json'); xobj.open('GET', './levels/" + selectedLevel + "/" + selectedLevel + ".json', false); xobj.onreadystatechange = function() {if (xobj.readyState == 4 && xobj.status == '200'){ json = xobj.responseText; }}; xobj.send(null); if (json != null) { var data = JSON.parse(json); for (var i in data) { localStorage.setItem(i, JSON.stringify(data[i])); } }";
                    js.ExecuteScript(scriptSetLocalStorage);
                }
                PromptView("edit");
            }
            else if (driver != null)
            {
                System.Windows.MessageBox.Show("Le driver est déjà lancé.");
            }
        }

        /// <summary>
        /// Action when clicking on 'import'.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { Filter = "All images (*jpg,*png,*jpeg,*gif,*html)|*jpg;*png;*jpeg;*gif;*html|JPG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|GIF Files (*.gif)|*.gif" };
            try
            {
                ofd.ShowDialog();
                fileToImport = ofd.FileName;
                if (string.IsNullOrEmpty(fileToImport))
                {
                    return;
                }
                string assetsPath = Path.Combine(root, assetsRelativPath);

                MessageBoxResult wantChangeFileName = System.Windows.MessageBox.Show("Voulez vous renommer le fichier ?", "Confirmer", MessageBoxButton.YesNo);
                if (wantChangeFileName == MessageBoxResult.Yes)
                {
                    PromptView("name");
                }
                else
                {
                    newFileName = Path.GetFileName(fileToImport);
                    CopyAndRename(assetsPath, false);
                }

            }
            catch { }
        }

        /// <summary>
        /// Action when clicking on 'ok' of the view of the choice of a new name for the file to import.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewNameOkDialogButton_Click(object sender, RoutedEventArgs e)
        {
            if (NewNameChoice.Text != null && NewNameChoice.Text != string.Empty)
            {
                newFileName = NewNameChoice.Text;
                if (NewNameChoice.Text.IndexOf('.') == -1)
                {
                    newFileName += Path.GetExtension(fileToImport);
                }
                string assetsPath = Path.Combine(root, assetsRelativPath);
                bool findNewName = CopyAndRename(assetsPath, true);
                if (findNewName)
                {
                    PromptView(precedentView);
                }
            }
        }

        /// <summary>
        /// Action when clicking on 'cancel' of the view of the choice of a new name for the file to import.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewNameCancelDialogButton_Click(object sender, RoutedEventArgs e)
        {
            NewNameChoice.Text = "";
            PromptView(precedentView);
        }

        /// <summary>
        /// Action when clicking on radiobutton 'training' of the view of selected level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainingRadioButton_Click(object sender, RoutedEventArgs e)
        {
            SetLevelTraining(true);
        }

        /// <summary>
        /// Action when clicking on radiobutton 'exam' of the view of selected level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExamRadioButton_Click(object sender, RoutedEventArgs e)
        {
            SetLevelTraining(false);
        }
    }
}
